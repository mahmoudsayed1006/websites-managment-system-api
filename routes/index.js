import express from 'express';
import userRoute from './user/user.route';
import ScreenRoute  from './screen/screen.route';
import VideoRoute  from './video/video.route';
import WebsiteRoute  from './website/website.route';
import DepartmentRoute  from './department/department.route';

const router = express.Router();

router.use('/', userRoute);
router.use('/screen',ScreenRoute);
router.use('/video',VideoRoute);
router.use('/website',WebsiteRoute);
router.use('/department',DepartmentRoute);

export default router;
