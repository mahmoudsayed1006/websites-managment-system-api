import mongoose,{ Schema} from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
const WebsiteSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    link:{
        type:String,
        required: true
    },
    department:{
        type:Number, 
        ref:'department',
        default:1
    },
    title:{
        type:String,
        default:""
    },
    deleted:{
        type:Boolean,
        default:false
    }
}, { timestamps: true });

WebsiteSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;
       
    }
});
autoIncrement.initialize(mongoose.connection);
WebsiteSchema.plugin(autoIncrement.plugin, { model: 'website', startAt: 1 });

export default mongoose.model('website', WebsiteSchema);