import ApiResponse from "../../helpers/ApiResponse";
import Video from "../../models/video/video.model";
import ApiError from '../../helpers/ApiError';
import { checkExist, checkExistThenGet} from "../../helpers/CheckMethods";
import { checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";

export default {

    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = { deleted: false };
            let Videos = await Video.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const VideosCount = await Video.count(query);
            const pageCount = Math.ceil(VideosCount / limit);

            res.send(new ApiResponse(Videos, page, pageCount, limit, VideosCount, req));
        } catch (err) {
            next(err);
        }
    },

    validateBody(isUpdate = false) {
        let validations = [
            body('link').not().isEmpty().withMessage('link is required'),
        ];
        

        return validations;
    },

    async create(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            const validatedBody = checkValidations(req);
            let createdVideo = await Video.create({ ...validatedBody});

            res.status(201).send(createdVideo);
        } catch (err) {
            next(err);
        }
    },
    async update(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { VideoId } = req.params;
            await checkExist(VideoId, Video, { deleted: false });

            const validatedBody = checkValidations(req);
            let updatedVideo = await Video.findByIdAndUpdate(VideoId, {
                ...validatedBody,
            }, { new: true });
            res.status(200).send(updatedVideo);
        }
        catch (err) {
            next(err);
        }
    },
    async findById(req, res, next) {
        try {
            let { VideoId } = req.params;
            await checkExist(VideoId, Video, { deleted: false });
            let Video = await Video.findById(VideoId);
            res.send(Video);
        } catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
                
            let { VideoId } = req.params;
            let video = await checkExistThenGet(VideoId, Video, { deleted: false });
            
            video.deleted = true;
            await video.save();
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
};