import mongoose,{ Schema} from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
const ScreenSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    number:{
        type:String,
        required: true
    },
    webSite:{
        type:Number,
        required: true,
        ref:'website'
    },
    departmentId:{
        type:Number,
        ref:'department'
    },
    floor:{
        type:String,
        default:""
    },
    department:{
        type:String,
        default:""
    },
    intervals: [
        new Schema({
            from: { 
                type: Number,
                required: true,
            },
            to: {
                type: Number,
                required: true,
            },
            webSite: {
                type: String,
                required: true,
            },
        }, { _id: false })
    ],
    interval:{
        type:Boolean,
        default:false
    },
    webSites:{
        type:[Number],
    },
    time:{
        type: Number,
    },
    endTime:{
        type: Number,
    },
    random:{
        type:Boolean,
        default:false
    },
    selectedIndex:{
        type:Number,
        default:0
    },
    online:{
        type:Boolean,
        default:false
    },
    deleted:{
        type:Boolean,
        default:false
    }
}, { timestamps: true });

ScreenSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;

    }
});
autoIncrement.initialize(mongoose.connection);
ScreenSchema.plugin(autoIncrement.plugin, { model: 'screen', startAt: 1 });

export default mongoose.model('screen', ScreenSchema);