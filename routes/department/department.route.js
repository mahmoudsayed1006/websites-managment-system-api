import express from 'express';
import DepartmentController from '../../controllers/department/department.controller';
import { requireAuth } from '../../services/passport';

const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        DepartmentController.validateBody(),
        DepartmentController.create
    )
router.route('/get')
    .get(DepartmentController.findAll);  
router.route('/:DepartmentId')
    .get(DepartmentController.findById)
    .delete( requireAuth,DepartmentController.delete)
    .put(
        requireAuth,
        DepartmentController.validateBody(),
        DepartmentController.update
    );

export default router;