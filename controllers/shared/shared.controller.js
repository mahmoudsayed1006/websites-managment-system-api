import fs from 'fs';
import ApiError from '../../helpers/ApiError';
import { validationResult } from 'express-validator/check';
import { matchedData } from 'express-validator/filter';

export function checkValidations(req) {

  const validationErrors = validationResult(req).array({ onlyFirstError: true });

  if (validationErrors.length > 0) {
    throw new ApiError(422, validationErrors);
  }

  return matchedData(req);
}
