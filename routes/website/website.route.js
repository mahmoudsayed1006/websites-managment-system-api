import express from 'express';
import WebsiteController from '../../controllers/website/website.controller';
import { requireAuth } from '../../services/passport';

const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        WebsiteController.validateBody(),
        WebsiteController.create
    )
router.route('/get')
    .get(WebsiteController.findAll);  
router.route('/:WebsiteId')
    .get(WebsiteController.findById)
    .delete( requireAuth,WebsiteController.delete)
    .put(
        requireAuth,
        WebsiteController.validateBody(),
        WebsiteController.update
    );

export default router;