import express from 'express';
import ScreenController from '../../controllers/screen/screen.controller';
import { requireAuth } from '../../services/passport';
import { parseStringToArrayOfObjectsMw } from '../../utils';

const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        //parseStringToArrayOfObjectsMw('intervals'),
        ScreenController.validateBody(),
        ScreenController.create
    )
router.route('/get')
    .get(ScreenController.findAll);  
router.route('/:ScreenId')
    .get(ScreenController.findById)
    .delete( requireAuth,ScreenController.delete)
    .put(
        requireAuth,
        //parseStringToArrayOfObjectsMw('intervals'),
        ScreenController.validateBody(),
        ScreenController.update
    );

router.route('/:ScreenId/online')
    .put(
        requireAuth,
        ScreenController.online
    );
router.route('/:ScreenId/offline')
    .put(
        requireAuth,
        ScreenController.offline
    );
export default router;