import express from 'express';
import { requireSignIn, requireAuth} from '../../services/passport';
import UserController from '../../controllers/user/user.controller';

const router = express.Router();

router.post('/signin', requireSignIn, UserController.signIn);

router.route('/signup')
    .post(
        UserController.validateUserCreateBody(),
        UserController.signUp
    );
router.route('/getAll')
    .get(
        requireAuth,
        UserController.findAll
    );

router.route('/logout')
    .put(
        requireAuth,
        UserController.logout
    );

router.route('/:userId')
    .get(UserController.findById)
    .delete( requireAuth,UserController.delete);

router.put('/user/:userId/updateInfo',
    UserController.validateUpdatedBody(),
    UserController.updateInfo);

export default router;
