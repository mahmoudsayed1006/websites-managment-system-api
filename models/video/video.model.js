import mongoose,{ Schema} from "mongoose";
import autoIncrement from 'mongoose-auto-increment';
const VideoSchema = new Schema({
    _id: {
        type: Number,
        required: true
    },
    link:{
        type:String,
        required: true
    },
    deleted:{
        type:Boolean,
        default:false
    }
}, { timestamps: true });

VideoSchema.set('toJSON', {
    transform: function (doc, ret, options) {
        ret.id = ret._id;
        delete ret._id;
        delete ret.__v;

    }
});
autoIncrement.initialize(mongoose.connection);
VideoSchema.plugin(autoIncrement.plugin, { model: 'video', startAt: 1 });

export default mongoose.model('video', VideoSchema);