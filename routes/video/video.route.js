import express from 'express';
import VideoController from '../../controllers/video/video.controller';
import { requireAuth } from '../../services/passport';

const router = express.Router();

router.route('/')
    .post(
        requireAuth,
        VideoController.validateBody(),
        VideoController.create
    )
router.route('/get')
    .get(VideoController.findAll);  
router.route('/:VideoId')
    .get(VideoController.findById)
    .delete( requireAuth,VideoController.delete)
    .put(
        requireAuth,
        VideoController.validateBody(),
        VideoController.update
    );

export default router;