import { checkExistThenGet, checkExist } from '../../helpers/CheckMethods';
import ApiResponse from "../../helpers/ApiResponse";
import { body } from 'express-validator/check';
import { checkValidations} from '../shared/shared.controller';
import { generateToken } from '../../utils/token';
import User from "../../models/user/user.model";
import ApiError from '../../helpers/ApiError';
import Screen from "../../models/screen/screen.model";
import Website from "../../models/website/website.model";

const populateQuery = [
    {
        path: 'screen', model: 'screen',
        populate: { path: 'webSite', model: 'website' }
    },
    {
        path: 'screen', model: 'screen',
        populate: { path: 'webSites', model: 'website' }
    },
];

export default {
    async signIn(req, res, next) {
        try{
            let user = req.user;
           
            user = await User.findById(user.id).populate(populateQuery);
            let screen = await checkExistThenGet(user.screen.id, Screen, { deleted: false });
            let link = " ";
            var dt = new Date();
            var hours = dt.getHours();
            console.log(hours);
            let intervals = [];
            if(user.screen.interval === true){
                user.screen.intervals.forEach(interval => {
                    intervals.push(interval)
                }); 
                console.log(intervals);
                for(var i=0; i< intervals.length;i++){
                    console.log(intervals[i].from)
                    if(hours >=intervals[i].from && hours <= intervals[i].to){
                        link = intervals[i].webSite
                    }
                } 
            } 
            if(user.screen.random ===true){
                let websites = user.screen.webSites;
                let endIndex = websites.length - 1;
                console.log(endIndex)

                let nextIndex = user.screen.selectedIndex +1;
                console.log(nextIndex)
                var dt = new Date();
                let myDate = Date.parse(dt)
                console.log(myDate)
                console.log(user.screen.endTime)
                console.log(screen.time)
                console.log(myDate + screen.time * 1000)
                if(myDate > user.screen.endTime){
                    if(nextIndex <= endIndex){
                        link = websites[nextIndex].link;
                        screen.selectedIndex = nextIndex;
                        screen.endTime =  myDate + screen.time * 1000;
                        await screen.save();
                    } else{
                        screen.selectedIndex = 0;
                        screen.endTime =  myDate + screen.time * 1000;
                        link = websites[0].link;
                        await screen.save();
                    }
                } else{
                    link = websites[screen.selectedIndex].link
                }
            }
            else{
                link = user.screen.webSite.link
            }
            console.log(link)
            console.log(user.screen)
            if(user.screen){
                let screen = await checkExistThenGet(user.screen._id, Screen, { deleted: false });
                screen.online = true;
                await screen.save()

            }
            res.status(200).send({
                user,
                webSite:link,
                token: generateToken(user.id),
            });
            
        } catch(err){
            next(err);
        }
    },

    validateUserCreateBody(isUpdate = false) {
        let validations = [
            body('fullname').optional(),
            body('screen').not().isEmpty().withMessage('screen is required'),
            body('phone').optional()
                .custom(async (value, { req }) => {
                    let userQuery = { phone: value };
                    if (isUpdate && req.user.phone === value)
                        userQuery._id = { $ne: req.user._id };

                    if (await User.findOne(userQuery))
                        throw new Error('phone duplicated');
                    else
                        return true;  
                }),
            body('username').not().isEmpty().withMessage('username is required')
                .custom(async (value, { req }) => {
                    let userQuery = { username: value };
                    if (isUpdate && req.user.username === value)
                        userQuery._id = { $ne: req.user._id };

                    if (await User.findOne(userQuery))
                        throw new Error('username duplicated');
                    else
                        return true;
                }),
            body('type').not().isEmpty().withMessage('type is required')
                .isIn(['WORKER','ADMIN']).withMessage('wrong type'),
        ];
        if (!isUpdate) {
            validations.push([
                body('password').not().isEmpty().withMessage('password is required'),

            ]);
        }
        return validations;
    },
    async signUp(req, res, next) {
        try {
            const validatedBody = checkValidations(req);
            let createdUser = await User.create({
                ...validatedBody
            });
            res.status(201).send({
                user: await User.findOne(createdUser).populate(populateQuery),
                token: generateToken(createdUser.id)
            });

        } catch (err) {
            next(err);
        }
    },

    async findById(req, res, next) {
        try {
            let { userId } = req.params;
            await checkExist(userId, User, { deleted: false });

            let user = await User.findById(userId).populate(populateQuery);
            let screen = await checkExistThenGet(user.screen.id, Screen, { deleted: false });
            let link = "";
            var dt = new Date();
            var hours = dt.getHours();
            console.log(hours);
            let intervals = [];
            if(user.screen.interval === true){
                user.screen.intervals.forEach(interval => {
                    intervals.push(interval)
                }); 
                console.log(intervals);
                for(var i=0; i< intervals.length;i++){
                    console.log(intervals[i].from)
                    if(hours >=intervals[i].from && hours <= intervals[i].to){
                        link = intervals[i].webSite
                    }
                } 
            }
            if(user.screen.random ===true){
                let websites = user.screen.webSites;
                let endIndex = websites.length - 1;
                console.log(endIndex)

                let nextIndex = user.screen.selectedIndex +1;
                console.log(nextIndex)
                var dt = new Date();
                let myDate = Date.parse(dt)
                console.log(myDate)
                console.log(user.screen.endTime)
                console.log(screen.time)
                console.log(myDate + screen.time * 1000)
                if(myDate > user.screen.endTime){
                    if(nextIndex <= endIndex){
                        link = websites[nextIndex].link;
                        screen.selectedIndex = nextIndex;
                        screen.endTime =  myDate + screen.time * 1000;
                        await screen.save();
                    } else{
                        screen.selectedIndex = 0;
                        screen.endTime =  myDate + screen.time * 1000;
                        link = websites[0].link;
                        await screen.save();
                    }
                } else{
                    link = websites[screen.selectedIndex].link
                }
            }
             else{
                link = user.screen.webSite.link
            }
            console.log(link)
            res.status(200).send({
                user,
                webSite:link,
                token: generateToken(userId),
            });
        } catch (error) {
            next(error);
        }
    },
    validateUpdatedBody(isUpdate = true) {
        let validations = [
            body('fullname').optional(),
            body('screen').not().isEmpty().withMessage('screen is required'),
            body('phone').optional()
                .custom(async (value, { req }) => {
                    let userQuery = { phone: value };
                    let {userId} = req.params;
                    userQuery._id = { $ne: userId };
                    if (await User.findOne(userQuery))
                        throw new Error(req.__('phone duplicated'));
                    else
                        return true;
                }),
            body('username').not().isEmpty().withMessage('username is required')
                .custom(async (value, { req }) => {
                    let userQuery = { username: value };
                    let {userId} = req.params;
                    userQuery._id = { $ne: userId };
                    if (await User.findOne(userQuery))
                        throw new Error(req.__('username duplicated'));
                    else
                        return true;
                }),
            ];
        return validations;
    },
    async updateInfo(req, res, next) {
        try {
            let {userId} = req.params;
            const validatedBody = checkValidations(req);
            let user = await checkExistThenGet(userId, User);
            if(validatedBody.phone){
                user.phone = validatedBody.phone;
            }
            if(validatedBody.screen){
                user.screen = validatedBody.screen;
            }
            if(validatedBody.fullname){
                user.fullname = validatedBody.fullname;
            }
            if (req.body.password) {
                user.password = req.body.password;
           }
            await user.save();
            res.status(200).send({
                user: await User.findById(userId).populate(populateQuery)
            });

        } catch (error) {
            next(error);
        }
    },
  
    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            {type} = req.query;
            let query = { deleted: false };
            if(type) query.type = type;
            let users = await User.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const usersCount = await User.count(query);
            const pageCount = Math.ceil(usersCount / limit);

            res.send(new ApiResponse(users, page, pageCount, limit, usersCount, req));
        } catch (err) {
            next(err);
        }
    }, 
    async delete(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
            let { userId } = req.params;
            let users = await checkExistThenGet(userId, User, { deleted: false });
            users.deleted = true;
            await users.save();
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
    async logout(req, res, next) {
        try {
            let user = await checkExistThenGet(req.user._id, User, { deleted: false });
            let screen = await checkExistThenGet(user.screen, Screen, { deleted: false });
            screen.online = false;
            await screen.save();
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
    

};
