import ApiResponse from "../../helpers/ApiResponse";
import Screen from "../../models/screen/screen.model";
import User from "../../models/user/user.model";
import ApiError from '../../helpers/ApiError';
import { checkExist, checkExistThenGet} from "../../helpers/CheckMethods";
import { checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";
import Department from "../../models/department/department.model";

const populateQuery = [
    { path: 'webSite', model: 'website' },
    { path: 'webSites', model: 'website' },
];
export default {

    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            { department,search} = req.query;
            let query = {deleted: false };

            if (department) query.department = { $regex: '.*' + department + '.*' };
            if(search){
                query = {
                    $and: [
                        { $or: [
                            {department: { $regex:'.*' + search + '.*' }},
                            {number: { $regex:'.*' + search + '.*' }},                         
                          
                          ] 
                        },
                        {deleted: false} 
                    ]
                };
            }
            
            
            let Screens = await Screen.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const ScreensCount = await Screen.count(query);
            const pageCount = Math.ceil(ScreensCount / limit);

            res.send(new ApiResponse(Screens, page, pageCount, limit, ScreensCount, req));
        } catch (err) {
            next(err);
        }
    },

    validateBody(isUpdate = false) {
        let validations = [
            body('number').not().isEmpty().withMessage('number is required'),
            body('webSite').not().isEmpty().withMessage('website link is required'),
            body('floor').optional(),
            body('department').optional(),
            body('departmentId').not().isEmpty().withMessage('department is required'),
            body('intervals')
            .optional()
            //.custom(vals => isArray(vals)).withMessage('intervals should be an array')
            .isLength({ min: 5 }).withMessage('intervals should have at least one element of branches')
            .custom(async (properties, { req }) => {
                for (let prop of properties) {
                    body('from').not().isEmpty().withMessage('from is required'),
                    body('to').not().isEmpty().withMessage('to is required'),
                    body('webSite').not().isEmpty().withMessage('webSite is required')
                }
                return true;
            }),
            body('webSites').optional(),
            body('time').optional(),

        ];
        

        return validations;
    }, 

    async create(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            const validatedBody = checkValidations(req);
            console.log(validatedBody.intervals)
            if(validatedBody.webSites){
                validatedBody.random = true
                validatedBody.interval = false
                var t = new Date();
                t.setSeconds(t.getSeconds() + parseInt(validatedBody.time));
                console.log(t)
                let endDateMillSec = Date.parse(t);
                console.log(endDateMillSec)
                validatedBody.endTime = endDateMillSec;
            }
            if(validatedBody.intervals){
                validatedBody.interval = true
                validatedBody.random = false
            }
            if(validatedBody.departmentId){
                let department = await checkExistThenGet(validatedBody.departmentId,Department, { deleted: false });
                validatedBody.department = department.name
            }
            let createdScreen = await Screen.create({ ...validatedBody});

            res.status(201).send(createdScreen);
        } catch (err) {
            next(err);
        }
    },
    async update(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { ScreenId } = req.params;
            await checkExist(ScreenId, Screen, { deleted: false });

            const validatedBody = checkValidations(req);
            if(validatedBody.webSites){
                validatedBody.random = true
                validatedBody.interval = false
                var t = new Date();
                t.setSeconds(t.getSeconds() + parseInt(validatedBody.time));
                console.log(t)
                let endDateMillSec = Date.parse(t);
                console.log(endDateMillSec)
                validatedBody.endTime = endDateMillSec;
            }
            if(validatedBody.intervals){
                validatedBody.interval = true
                validatedBody.random = false
            }
            let updatedScreen = await Screen.findByIdAndUpdate(ScreenId, {
                ...validatedBody,
            }, { new: true });
            res.status(200).send(updatedScreen);
        }
        catch (err) {
            next(err);
        }
    },
    async findById(req, res, next) {
        try {
            let { ScreenId } = req.params;
            await checkExist(ScreenId, Screen, { deleted: false });
            let Screen = await Screen.findById(ScreenId);
            res.send(Screen);
        } catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
                
            let { ScreenId } = req.params;
            let screen = await checkExistThenGet(ScreenId, Screen, { deleted: false });
            let users = await User.find({ screen: ScreenId });
            for (let user of users ) {
                user.deleted = true;
                await user.save();
            }
            screen.deleted = true;
            await screen.save();
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
    async online(req, res, next) {
        try {
            let { ScreenId } = req.params;
            let screen = await checkExistThenGet(ScreenId, Screen, { deleted: false });
            screen.online = true;
            await screen.save();
            res.status(204).send('online');

        }
        catch (err) {
            next(err);
        }
    },
    async offline(req, res, next) {
        try {
            let { ScreenId } = req.params;
            let screen = await checkExistThenGet(ScreenId, Screen, { deleted: false });
            screen.online = false;
            await screen.save();
            res.status(204).send('offline');

        }
        catch (err) {
            next(err);
        }
    },
};