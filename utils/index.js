
import ApiError from '../helpers/ApiError';

export * from './token';

export function parseStringToArrayOfObjectsMw(fieldName, inWhich = 'body') {
  return (req, res, next) => {
      try {
          if (req[inWhich][fieldName]) {
            let arrOfObjectsAsString = req[inWhich][fieldName];
            console.log('the object',typeof arrOfObjectsAsString);
            let handledStringForParsing = arrOfObjectsAsString.replace(/([a-zA-Z0-9]+?):/g, '"$1":').replace(/'/g, '"');
            console.log('the object',handledStringForParsing);
            console.log('the object 333',JSON.parse(handledStringForParsing));
              req[inWhich][fieldName] = JSON.parse(handledStringForParsing);
          }
          next();
      } catch (err) {
          console.log(err);
          next(new ApiError(400, { message: `Failed To Parse "${fieldName}"` }));
      }
  }
}
