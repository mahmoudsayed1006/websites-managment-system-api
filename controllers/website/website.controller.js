import ApiResponse from "../../helpers/ApiResponse";
import Website from "../../models/website/website.model";
import ApiError from '../../helpers/ApiError';
import { checkExist, checkExistThenGet} from "../../helpers/CheckMethods";
import { checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";
const populateQuery = [
    { path: 'department', model: 'department' },
];
export default {

    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20,
            {department} = req.query;
            let query = { deleted: false };
            if(department) query.department = department;
            let Websites = await Website.find(query).populate(populateQuery)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const WebsitesCount = await Website.count(query);
            const pageCount = Math.ceil(WebsitesCount / limit);

            res.send(new ApiResponse(Websites, page, pageCount, limit, WebsitesCount, req));
        } catch (err) {
            next(err);
        }
    },

    validateBody(isUpdate = false) {
        let validations = [
            body('link').not().isEmpty().withMessage('link is required')
            .isURL().withMessage('incorrect url'),
            body('department').not().isEmpty().withMessage('department is required'),
            body('title').not().isEmpty().withMessage('display name is required'),

        ];
        

        return validations;
    },

    async create(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            const validatedBody = checkValidations(req);
            let createdWebsite = await Website.create({ ...validatedBody});

            res.status(201).send(createdWebsite);
        } catch (err) {
            next(err);
        }
    },
    async update(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { WebsiteId } = req.params;
            await checkExist(WebsiteId, Website, { deleted: false });

            const validatedBody = checkValidations(req);
            let updatedWebsite = await Website.findByIdAndUpdate(WebsiteId, {
                ...validatedBody,
            }, { new: true });
            res.status(200).send(updatedWebsite);
        }
        catch (err) {
            next(err);
        }
    },
    async findById(req, res, next) {
        try {
            let { WebsiteId } = req.params;
            await checkExist(WebsiteId, Website, { deleted: false });
            let website = await Website.findById(WebsiteId);
            res.send(website);
        } catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
                
            let { WebsiteId } = req.params;
            let website = await checkExistThenGet(WebsiteId, Website, { deleted: false });
            
            website.deleted = true;
            await website.save();
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
};