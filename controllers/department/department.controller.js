import ApiResponse from "../../helpers/ApiResponse";
import Department from "../../models/department/department.model";
import ApiError from '../../helpers/ApiError';
import { checkExist, checkExistThenGet} from "../../helpers/CheckMethods";
import { checkValidations } from "../shared/shared.controller";
import { body } from "express-validator/check";

export default {

    async findAll(req, res, next) {

        try {
            let page = +req.query.page || 1, limit = +req.query.limit || 20;
            let query = { deleted: false };
            let Departments = await Department.find(query)
                .sort({ createdAt: -1 })
                .limit(limit)
                .skip((page - 1) * limit);


            const DepartmentsCount = await Department.count(query);
            const pageCount = Math.ceil(DepartmentsCount / limit);

            res.send(new ApiResponse(Departments, page, pageCount, limit, DepartmentsCount, req));
        } catch (err) {
            next(err);
        }
    },

    validateBody(isUpdate = false) {
        let validations = [
            body('name').not().isEmpty().withMessage('department name is required'),
        ];
        

        return validations;
    },

    async create(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            const validatedBody = checkValidations(req);
            let createdDepartment = await Department.create({ ...validatedBody});

            res.status(201).send(createdDepartment);
        } catch (err) {
            next(err);
        }
    },
    async update(req, res, next) {

        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));

            let { DepartmentId } = req.params;
            await checkExist(DepartmentId, Department, { deleted: false });

            const validatedBody = checkValidations(req);
            let updatedDepartment = await Department.findByIdAndUpdate(DepartmentId, {
                ...validatedBody,
            }, { new: true });
            res.status(200).send(updatedDepartment);
        }
        catch (err) {
            next(err);
        }
    },
    async findById(req, res, next) {
        try {
            let { DepartmentId } = req.params;
            await checkExist(DepartmentId, Department, { deleted: false });
            let department = await Department.findById(DepartmentId);
            res.send(department);
        } catch (err) {
            next(err);
        }
    },

    async delete(req, res, next) {
        try {
            let user = req.user;
            if (user.type != 'ADMIN')
                return next(new ApiError(403, ('admin.auth')));
                
            let { DepartmentId } = req.params;
            let department = await checkExistThenGet(DepartmentId, Department, { deleted: false });
            
            department.deleted = true;
            await department.save();
            res.status(204).send('delete success');

        }
        catch (err) {
            next(err);
        }
    },
};